package api;

import client.Entity.Drugstore;
import client.Repository.DrugstoreRepository;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Class QuotationResource
 * Created by Alexis on 23/04/2019
 */
@Path("/drugstore")
public class DrugstoreResource implements Resource<Drugstore> {

    @EJB
    private DrugstoreRepository drugstoreRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Drugstore> getAll() {
        return drugstoreRepository.getAll();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public Drugstore get(@PathParam("id") long id) {
        return drugstoreRepository.get(id);
    }

    @POST
    public Drugstore create() {
        return null;
    }

    @PUT
    public Drugstore update() {
        return null;
    }
}
