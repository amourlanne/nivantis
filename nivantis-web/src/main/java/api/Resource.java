package api;

import client.Entity.Product;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

public interface Resource <T> {

    List<T> getAll();

    T get(long id);

    T create();

    T update();
}
