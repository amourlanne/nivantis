package api;

import client.Entity.Quotation;
import client.Repository.QuotationRepository;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Class QuotationResource
 * Created by Alexis on 23/04/2019
 */
@Path("/quotations")
public class QuotationResource implements Resource<Quotation> {

    @EJB
    private QuotationRepository quotationRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Quotation> getAll() {
        return quotationRepository.getAll();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public Quotation get(@PathParam("id") long id) {
        return quotationRepository.get(id);
    }

    @POST
    public Quotation create() {
        return null;
    }

    @PUT
    public Quotation update() {
        return null;
    }
}
