package api;

import client.Entity.Product;
import client.Repository.ProductRepository;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Class ProductResource
 * Created by Alexis on 12/04/2019
 */
@Path("/products")
public class ProductResource implements Resource<Product> {

    @EJB
    private ProductRepository productRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public Product get(@PathParam("id") long id) {
        return productRepository.get(id);
    }

    @POST
    public Product create() {
        return null;
    }

    @PUT
    public Product update() {
        return null;
    }
}
