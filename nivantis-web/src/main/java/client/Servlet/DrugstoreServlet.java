package client.Servlet;


import client.Entity.Quotation;
import client.Exception.InvalidFormException;
import client.Repository.DrugstoreRepository;
import client.Repository.ProductRepository;
import client.Repository.QuotationRepository;
import client.Service.QuotationService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/drugstores")
public class DrugstoreServlet extends HttpServlet implements CRUDServlet {
	
	@EJB
	private DrugstoreRepository drugstoreRepository;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        dispatchActions(request, response);
    }

    @Override
    public void doGetShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String id = request.getParameter("id");
        request.setAttribute("drugstore", drugstoreRepository.get(Long.parseLong(id)));

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/drugstore/show.jsp" ).forward( request, response );
    }

    @Override
    public void doGetEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        this.doGetDefault(request, response);
    }

    @Override
    public void doGetNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        this.doGetDefault(request, response);
    }

    @Override
    public void doGetDefault(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("drugstores", drugstoreRepository.getAll());
        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/drugstore/list.jsp" ).forward( request, response );
    }
}
