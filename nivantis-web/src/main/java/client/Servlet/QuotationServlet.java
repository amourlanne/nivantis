package client.Servlet;


import client.Entity.Drugstore;
import client.Entity.Product;
import client.Entity.Quotation;
import client.Entity.QuotationLine;
import client.Exception.InvalidFormException;
import client.Repository.DrugstoreRepository;
import client.Repository.ProductRepository;
import client.Repository.QuotationRepository;
import client.Service.ProductService;
import client.Service.QuotationLineService;
import client.Service.QuotationService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/quotations")
public class QuotationServlet extends HttpServlet implements CRUDServlet {
	
	@EJB
	private QuotationRepository quotationRepository;

    @EJB
    private ProductRepository productRepository;

    @EJB
    private DrugstoreRepository drugstoreRepository;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        dispatchActions(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String ref = request.getParameter("ref");
        String productId = request.getParameter("productId");
        String drugstoreId = request.getParameter("drugstoreId");
        String discountRate = request.getParameter("discountRate");
        String quantity = request.getParameter("quantity");

        try {
            Product product = productRepository.get(Long.parseLong(productId));
            Drugstore drugstore = drugstoreRepository.get(Long.parseLong(drugstoreId));

            QuotationLine quotationLine = QuotationLineService.register(product, Float.parseFloat(discountRate), Integer.parseInt(quantity));

            List<QuotationLine> quotationLines = new ArrayList<>();
            quotationLines.add(quotationLine);

            Quotation quotation = QuotationService.register(ref, quotationLines, drugstore);
            quotationRepository.add(quotation);

        } catch (InvalidFormException e) {
            request.setAttribute("errors", e.getMessages());
        }

        this.doGetNew(request, response);
    }

    @Override
    public void doGetShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String id = request.getParameter("id");
        request.setAttribute("quotation", quotationRepository.get(Long.parseLong(id)));

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/quotation/show.jsp" ).forward( request, response );
    }

    @Override
    public void doGetEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/quotation/edit.jsp" ).forward( request, response );
    }

    @Override
    public void doGetNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("products", productRepository.getAll());
        request.setAttribute("drugstores", drugstoreRepository.getAll());

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/quotation/new.jsp" ).forward( request, response );
    }

    @Override
    public void doGetDefault(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("quotationList", quotationRepository.getAll());
        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/quotation/list.jsp" ).forward( request, response );
    }
}
