package client.Servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class CRUDServlet
 * Created by Alexis on 23/04/2019
 */
public interface CRUDServlet {
     static final String ACTION_SHOW  = "ACTION_SHOW";
     static final String ACTION_EDIT  = "ACTION_EDIT";
     static final String ACTION_NEW  = "ACTION_NEW";

     default void dispatchActions(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

         String action = request.getParameter("action");

         if (action == null) {
             doGetDefault(request, response);
             return;
         }

        switch(action) {
            case CRUDServlet.ACTION_SHOW:
                doGetShow(request, response);
                break;
            case CRUDServlet.ACTION_EDIT:
                doGetEdit(request, response);
                break;
            case CRUDServlet.ACTION_NEW:
                doGetNew(request, response);
                break;
            default:
                doGetDefault(request, response);
        }
    }

      void doGetShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
      void doGetEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
      void doGetNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
      void doGetDefault(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
