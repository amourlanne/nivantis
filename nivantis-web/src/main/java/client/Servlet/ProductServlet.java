package client.Servlet;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import client.Entity.Product;
import client.Exception.InvalidFormException;
import client.Repository.ProductRepository;
import client.Service.ProductService;

import java.io.IOException;

@WebServlet("/products")
public class ProductServlet extends HttpServlet implements CRUDServlet {
	
	@EJB
	private ProductRepository productRepository;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        dispatchActions(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String ref = request.getParameter("ref");
        String prix = request.getParameter("prix");

        try {
            Product product = ProductService.register(name, ref, prix);
            productRepository.add(product);
        } catch (InvalidFormException e) {
            request.setAttribute("errors", e.getMessages());
        }

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/product/new.jsp" ).forward( request, response );
    }

    @Override
    public void doGetShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String id = request.getParameter("id");
        request.setAttribute("product", productRepository.get(Long.parseLong(id)));

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/product/show.jsp" ).forward( request, response );
    }

    @Override
    public void doGetEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/product/edit.jsp" ).forward( request, response );
    }

    @Override
    public void doGetNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/product/new.jsp" ).forward( request, response );
    }

    @Override
    public void doGetDefault(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("products", productRepository.getAll());
        this.getServletContext().getRequestDispatcher( "/WEB-INF/jsp/product/list.jsp" ).forward( request, response );
    }
}
