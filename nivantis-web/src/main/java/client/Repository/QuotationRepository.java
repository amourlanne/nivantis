package client.Repository;

import client.Entity.Quotation;

import javax.ejb.Stateless;
import java.util.List;

/**
 * Class QuotationRepository
 * Created by Alexis on 23/04/2019
 */
@Stateless
public class QuotationRepository extends AbstractRepository<Quotation> {
    @Override
    public Quotation get(long identifier) {
        return this.em.find(Quotation.class, identifier);
    }

    @Override
    public List<Quotation> getAll() {
        return this.em.createQuery("select q from Quotation q ", Quotation.class)
                .getResultList();
    }

    @Override
    public void add(Quotation item) {
        this.em.persist(item);
    }

    @Override
    public void update(Quotation item) {

    }

    @Override
    public void remove(Quotation item) {

    }
}
