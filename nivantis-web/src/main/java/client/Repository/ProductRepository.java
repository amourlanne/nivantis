package client.Repository;

import java.util.List;

import client.Entity.Product;

import javax.ejb.Stateless;

/**
 * Class ProductRepository
 * Created by Alexis on 12/04/2019
 */
@Stateless
public class ProductRepository extends AbstractRepository<Product> {

	@Override
	public Product get(long identifier) {
		return this.em.find(Product.class, identifier);
	}

	@Override
	public List<Product> getAll() {
		return this.em.createQuery("select p from Product p ", Product.class)
				.getResultList();
	}

	@Override
	public void add(Product item) {
		this.em.persist(item);
	}

	@Override
	public void update(Product item) {
		this.em.persist(item);
	}

	@Override
	public void remove(Product item) {
		this.em.remove(item);
	}
}
