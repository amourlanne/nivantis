package client.Repository;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Class AbstractRepository
 * Created by Alexis on 23/04/2019
 */

public abstract class AbstractRepository<T> {

    @PersistenceContext(unitName="nivantis")
    protected EntityManager em;

    public abstract T get(long identifier);

    public abstract List<T> getAll();

    public abstract void add(T item);

    public abstract void update(T item);

    public abstract void remove(T item);
}
