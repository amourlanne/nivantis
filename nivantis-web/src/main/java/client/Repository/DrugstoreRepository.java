package client.Repository;

import client.Entity.Drugstore;
import client.Entity.Quotation;

import javax.ejb.Stateless;
import java.util.List;

/**
 * Class QuotationRepository
 * Created by Alexis on 23/04/2019
 */
@Stateless
public class DrugstoreRepository extends AbstractRepository<Drugstore> {

    @Override
    public Drugstore get(long identifier) {
        return this.em.find(Drugstore.class, identifier);
    }

    @Override
    public List<Drugstore> getAll() {
        return this.em.createQuery("select d from Drugstore d ", Drugstore.class)
                .getResultList();
    }

    @Override
    public void add(Drugstore item) {
        this.em.persist(item);
    }

    @Override
    public void update(Drugstore item) {

    }

    @Override
    public void remove(Drugstore item) {

    }
}
