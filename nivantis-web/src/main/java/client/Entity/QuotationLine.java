package client.Entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class QuotationLine
 * Created by Alexis on 23/04/2019
 */
@XmlRootElement
@Entity
@IdClass(QuotationLineId.class)
public class QuotationLine {

    @Id
    @ManyToOne
    @JoinColumn(name="quotationId", nullable=false)
    @XmlTransient
    private Quotation quotation;

    @Id
    @ManyToOne
    @JoinColumn(name="productId", nullable=false)
    private Product product;

    @Column(nullable = true)
    private float discountRate = 0;

    private int quantity;

    public QuotationLine() {
    }

    public QuotationLine( Product product, float discountRate, int quantity) {
        this.product = product;
        this.discountRate = discountRate;
        this.quantity = quantity;
    }

    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public float getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(float discountRate) {
        this.discountRate = discountRate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
