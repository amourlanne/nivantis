package client.Entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class QuotationLineId
 * Created by Alexis on 23/04/2019
 */
public class QuotationLineId implements Serializable {

    public Quotation quotation;
    public Product product;

    @Override
    public boolean equals(Object o) {
        QuotationLineId that = (QuotationLineId) o;
        return (this.quotation.getId() == that.quotation.getId())
                && (this.product.getId() == that.product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(quotation, product);
    }
}
