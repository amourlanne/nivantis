package client.Entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Quotation
 * Created by Alexis on 23/04/2019
 */
@XmlRootElement
@Entity
public class Quotation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String ref;

    @OneToMany(fetch=FetchType.EAGER, mappedBy="quotation", cascade={/*CascadeType.REMOVE, CascadeType.PERSIST, */CascadeType.ALL })
    private List<QuotationLine> quotationLines  = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="drugstoreId")
    @XmlTransient
    private Drugstore drugstore;

    public Quotation() {
    }

    public Quotation(String ref, List<QuotationLine> quotationLines, Drugstore drugstore) {
        this.ref = ref;
        for(QuotationLine quotationLine : quotationLines ) {
            quotationLine.setQuotation(this);
        }
        this.quotationLines = quotationLines;
        this.drugstore = drugstore;
    }

    public long getId() {
        return id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public List<QuotationLine> getQuotationLines() {
        return quotationLines;
    }

    public void setQuotationLines(List<QuotationLine> quotationLines) {
        this.quotationLines = quotationLines;
    }

    public Drugstore getDrugstore() {
        return drugstore;
    }

    public void setDrugstore(Drugstore drugstore) {
        this.drugstore = drugstore;
    }
}
