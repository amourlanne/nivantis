package client.Entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Drugstore
 * Created by Alexis on 23/04/2019
 */
@XmlRootElement
@Entity
public class Drugstore {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String name;
    private String address;
    private String refGoogleAPI;

    @OneToMany(fetch=FetchType.EAGER, mappedBy="drugstore", cascade={/*CascadeType.REMOVE, CascadeType.PERSIST, */CascadeType.ALL })
    private List<Quotation> quotations  = new ArrayList<>();

    public Drugstore() {
    }

    public Drugstore(String name, String address, String refGoogleAPI) {
        this.name = name;
        this.address = address;
        this.refGoogleAPI = refGoogleAPI;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRefGoogleAPI() {
        return refGoogleAPI;
    }

    public void setRefGoogleAPI(String refGoogleAPI) {
        this.refGoogleAPI = refGoogleAPI;
    }

    public List<Quotation> getQuotations() {
        return quotations;
    }

    public void setQuotations(List<Quotation> quotations) {
        this.quotations = quotations;
    }

    public void addQuotation(Quotation quotation) {
        this.quotations.add(quotation);
    }

    public void removeQuotation(Quotation quotation) {
        this.quotations.remove(quotation);
    }
}
