package client.Service;

import client.Entity.Drugstore;
import client.Entity.Quotation;
import client.Entity.QuotationLine;
import client.Exception.InvalidFormException;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ProductService
 * Created by Alexis on 10/04/2019
 */
public class DrugstoreService {

    public static Drugstore register(String name, String address, String refGoogleAPI) throws InvalidFormException {
        InvalidFormException ex = new InvalidFormException();

        // TODO: test args

        if (ex.mustBeThrown()) {
            throw ex;
        }

        return new Drugstore(name, address, refGoogleAPI);
    }
}
