package client.Service;

import client.Entity.Product;
import client.Exception.InvalidFormException;
import client.Repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ProductService
 * Created by Alexis on 10/04/2019
 */
public class ProductService {

    public static Product register(String name, String ref, String prix) throws InvalidFormException {
        InvalidFormException ex = new InvalidFormException();
        Float floatPrix = null;

        if(name == null || name.equals("")) {
            ex.addMessage("name","Name is required.");
        }

        if(ref == null || ref.equals("")) {
            ex.addMessage("ref", "Reference is required.");
        }

        if(prix == null) {
            ex.addMessage("prix", "Prix is required.");
        } else {
            try {
                floatPrix = Float.parseFloat(prix);
            } catch (NumberFormatException e){
                ex.addMessage("prix", "Format invalid.");
            }
        }

        if (ex.mustBeThrown()) {
            throw ex;
        }

        return new Product(name, ref, floatPrix);
    }
}
