package client.Service;

import client.Entity.Product;
import client.Entity.Quotation;
import client.Entity.QuotationLine;
import client.Exception.InvalidFormException;

/**
 * Class ProductService
 * Created by Alexis on 10/04/2019
 */
public class QuotationLineService {

    public static QuotationLine register( Product product, float discountRate, int quantity) throws InvalidFormException {
        InvalidFormException ex = new InvalidFormException();

        // TODO: test args
        if (ex.mustBeThrown()) {
            throw ex;
        }

        return new QuotationLine(product, discountRate, quantity);
    }
}
