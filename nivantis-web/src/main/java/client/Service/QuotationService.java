package client.Service;

import client.Entity.Drugstore;
import client.Entity.Product;
import client.Entity.Quotation;
import client.Entity.QuotationLine;
import client.Exception.InvalidFormException;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ProductService
 * Created by Alexis on 10/04/2019
 */
public class QuotationService {

    public static Quotation register(String ref, List<QuotationLine> quotationLines, Drugstore drugstore) throws InvalidFormException {
        InvalidFormException ex = new InvalidFormException();

        // TODO: getQuotationsLines
        // TODO: test args

        if (ex.mustBeThrown()) {
            throw ex;
        }

        return new Quotation(ref, quotationLines, drugstore);
    }
}
