<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Drugstore | Nivantis</title>
</head>
<body>
<ul>
    <li><a href="./">Home</a></li>
    <li><a href="drugstores?action=ACTION_LIST">List drugstores</a></li>
</ul>

<p><c:out value="${ drugstore.name }"/> </p>
<p><c:out value="${ drugstore.address }"/> </p>
<p><c:out value="${ drugstore.refGoogleAPI }"/> </p>

</body>
</html>
