<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Drugstores | Nivantis</title>
</head>
<body>
<ul>
	<li><a href="./">Home</a></li>
</ul>
<table>
	<c:forEach items="${ drugstores }" var="d">
		<tr>
			<td><a href="drugstores?action=ACTION_SHOW&id=${ d.id }"><c:out value="${ d.name }"/></a></td>
			<td><c:out value="${ d.address }"/></td>
			<td><c:out value="${ d.refGoogleAPI }"/></td>
		</tr>
	</c:forEach>
</table>
</body>
</html>
