<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Quotations | Nivantis</title>
</head>
<body>
<ul>
    <li><a href="./">Home</a></li>
    <li><a href="quotations?action=ACTION_NEW">New quotation</a></li>
</ul>

<p><c:out value="${ quotationList.size() }"/> Quotations</p>
<table>
    <c:forEach items="${ quotationList }" var="q">
        <tr>
            <td><a href="quotations?action=ACTION_SHOW&id=${ q.id }"><c:out value="${ q.ref }"/></a> (<c:out value="${ q.quotationLines.size() }"/> products)</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
