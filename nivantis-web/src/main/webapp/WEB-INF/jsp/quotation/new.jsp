<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Quotation New | Nivantis</title>
</head>
<body>

<form name="quotation" method="post">
    <p>Reference: <input type="text" name="ref"></p>
    <p>
        <select id="drugstores" name="drugstoreId">
            <c:forEach items="${ drugstores }" var="d">
                <option value="<c:out value="${ d.id }"/>"><c:out value="${ d.name }"/></option>
            </c:forEach>
        </select>
    </p>
    <table>
        <tr>
            <td>
                <select id="products" name="productId">
                <c:forEach items="${ products }" var="p">
                    <option value="<c:out value="${ p.id }"/>"><c:out value="${ p.name }"/></option>
                </c:forEach>
            </select>
            </td>
            <td>DiscountRate: <input type="text" name="discountRate"></td>
            <td>Quantity: <input type="text" name="quantity"></td>
            <td><button>Add quotationLine</button></td>
        </tr>
    </table>
    <p><input type="submit" value="Add"></p>
</form>

</body>
</html>
