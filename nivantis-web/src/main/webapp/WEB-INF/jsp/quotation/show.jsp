<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Quotation | Nivantis</title>
</head>
<body>
<ul>
    <li><a href="./">Home</a></li>
    <li><a href="quotations?action=ACTION_LIST">List quotations</a></li>
</ul>
<p><c:out value="${ quotation.ref }"/> </p>
<p>
    <table>
        <c:forEach items="${ quotation.quotationLines }" var="ql">
            <tr>
                <td><c:out value="${ ql.product.name }"/></td>
                <td><c:out value="${ ql.discountRate }"/></td>
                <td><c:out value="${ ql.quantity }"/></td>
            </tr>
        </c:forEach>
    </table>

</p>

</body>
</html>
