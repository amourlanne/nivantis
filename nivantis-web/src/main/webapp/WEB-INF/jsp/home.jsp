<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home | Nivantis</title>
</head>
<body>
<ul>
    <li>
        <a href="products">Products</a>
    </li>
    <li>
        <a href="quotations">Quotations</a>
    </li>
    <li>
        <a href="drugstores">Drugstores</a>
    </li>
</ul>
</body>
</html>
