<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Products | Nivantis</title>
</head>
<body>
<ul>
	<li><a href="./">Home</a></li>
	<li><a href="products?action=ACTION_NEW">New product</a></li>
</ul>
<table>
	<c:forEach items="${ products }" var="p">
		<tr>
			<td><a href="products?action=ACTION_SHOW&id=${ p.id }"><c:out value="${ p.name }"/></a></td>
			<td><c:out value="${ p.ref }"/></td>
			<td><c:out value="${ p.prix }"/></td>
		</tr>
	</c:forEach>
</table>
</body>
</html>
