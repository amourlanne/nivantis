<%--
  Created by IntelliJ IDEA.
  User: Alexis
  Date: 12/04/2019
  Time: 12:42
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Products | Nivantis</title>
</head>
<body>
<ul>
    <li><a href="./">Home</a></li>
    <li><a href="products?action=ACTION_LIST">List products</a></li>
</ul>
<form name="produit" method="post">
    <p>Name: <input type="text" name="name"></p>
    <p>Reference: <input type="text" name="ref"></p>
    <p>Prix: <input type="number" name="prix"></p>
    <p><input type="submit" value="Ajouter"></p>
</form>
</body>
</html>
