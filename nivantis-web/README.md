# Nivantis Web

Nivantis web est une application web et API pour une pharmacie.

## Installation
Telecharger le [driver MySQL](https://mvnrepository.com/artifact/mysql/mysql-connector-java/5.1.47) et placer le .jar dans le dossier `standalone/deployments/` du serveur Wildfly.

## Configuration

Dans le fichier `standalone/configuration/standalone.xml`
``` xml
<datasource jta="true" jndi-name="java:/nivantis-data-source" pool-name="nivantis-data-source" enabled="true">
   <connection-url>jdbc:mysql://[HOST]:[PORT]/[NOM SCHEMA]</connection-url>
   <driver>mysql-connector-java-5.1.47.jar_com.mysql.jdbc.Driver_5_1</driver>
   <security>
       <user-name>[LOGIN]</user-name>
       <password>[PASSWORD]</password>
   </security>
</datasource>
```